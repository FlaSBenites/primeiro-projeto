import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css'
})
export class MenuComponent implements OnInit {

  public texto:string | undefined;
  constructor() {}



  ngOnInit(): void {
    this.texto = "Olá tudo bem?";
  }

  clicou() {
    this.texto = "clicou!";
  }

}
