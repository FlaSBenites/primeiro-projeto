import { Injectable } from '@angular/core';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor() { }

  public getUsuario() : Usuario{
    let usuario = new Usuario();
    usuario.nome = "Flávio"
    usuario.email = "flabenites@hotmail.com"

    return usuario;
  }

  public listaUsuario(): Usuario[] {
    return [
      {
        nome: "Flávio",
        email: "flabenites@hotmail.com"
      },
      {
        nome: "Erica",
        email: "ericabenites@hotmail.com"
      },
      {
        nome: "Pedro",
        email: "pedrobenites@hotmail.com"
      },
      {
        nome: "Laura",
        email: "laurabenites@hotmail.com"
      },
    ]
  }

}
